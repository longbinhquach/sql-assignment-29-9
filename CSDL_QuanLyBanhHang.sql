create database CustomerManagement

use CustomerManagement
go

-- Create and add records
create table KhachHang(
	MaKH int,
	HoTen nvarchar(50),
	DiaChi nvarchar(100),
	SoDT varchar(12),
	NgSinh datetime,
	DoanhSo money,
	NgDk datetime,
	primary key (MaKH)
)

create table NhanVien (
	MaNV int,
	HoTen nvarchar(50),
	SoDT varchar(12),
	NgVaoLam datetime,
	primary key (MaNV)
)

create table SanPham(
	MaSP int, 
	TenSP nvarchar(50),
	DVT varchar(10),
	NuocSX varchar(10),
	Gia money,
	primary key (MaSP)
)

create table HoaDon(
	SoHD int, 
	NgHD datetime, 
	MaKH int,
	MaNV int,
	TriGia int,
	primary key (SoHD),
	foreign key (MaKH) references KhachHang(MaKH),
	foreign key (MaNV) references NhanVien(MaNV)
)

create table CTHD(
	SoHD int,
	MaSP int,
	SL int,
	primary key (SoHD, MaSP),
	foreign key (SoHD) references HoaDon(SoHD),
	foreign key (MaSP) references SanPham(MaSP)
)

insert into KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, DoanhSo, NgDk) values 
	(1, 'Nguyen Van A', 'ABC', '0101010101', '2000-9-7', 200000, '2027-10-11'),
	(2, 'Nguyen Van B', 'ABC', '0201010101', '2000-12-7', 400000, '2027-10-11'),
	(3, 'Nguyen Van C', 'ABC', '0301010101', '2001-9-7', 250000, '2027-12-11')

insert into NhanVien(MaNV, HoTen, SoDT, NgVaoLam) values 
	(1, 'Tran Van A', '0202020202', '2019-1-1'),
	(2, 'Tran Van B', '0302020202', '2019-9-1'),
	(3, 'Tran Van C', '0402020202', '2019-11-11')

insert into SanPham(MaSP, TenSP, DVT, NuocSX, Gia) values 
	(1, 'Iphone', 'VND', 'US', 20000000),
	(2, 'Samsung Galaxy', 'VND', 'VN', 10000000),
	(3, 'Xiaomi Phone', 'VND', 'CN', 8000000),
	(4, 'HTC', 'VND', 'VN', 1000000)

insert into HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia) values 
	(1, '2022-9-7', 1, 2, 9000000),
	(2, '2022-1-7', 2, 3, 12000000),
	(3, '2023-5-23', 3, 1, 18000000)

insert into CTHD(SoHD, MaSP, SL) values 
	(1, 2, 100),
	(2, 3, 50),
	(3, 1, 120)

-- Question 1
select sp.MaSP, sp.TenSP
from SanPham sp join CTHD c on sp.MaSP = c.MaSP
	join HoaDon hd on hd.SoHD = c.SoHD
where sp.NuocSX = 'VN' or hd.NgHD = '2023-5-23'

-- Question 2
select sp.MaSP, sp.TenSP
from SanPham sp left join CTHD c on sp.MaSP = c.MaSP
where c.MaSP is null

-- Question 3
select hd.SoHD, hd.NgHD, c.SL
from HoaDon hd join CTHD c on hd.SoHD = c.SoHD
	join SanPham sp on sp.MaSP = c.MaSP
where sp.NuocSX = 'VN'
